# Markdown-P9

__Created 25/01/2019__

It's my practice of using [markdowns](https://openclassrooms.com/fr/courses/1304236-redigez-en-markdown).

So, it's something like description of this thing.  

In this file I will use:

1. Ordered list
2. *Italic text*
3. **Bold text**
4. ***Strong text*** 
5. and something else.

Like images of degu!![degu](https://www.woopets.fr/assets/races/000/408/og-image/octodon.jpg) 

----------

He is very beautiful. And this line is after "barre de séparation" (I didn't find his analog in English).

**And this is a part of code:**

Voici un code en C :

    int main()
    {
        printf("Hello world!\n");
        return 0;
    }
****



***FINISH***

![](http://bestanimations.com/Holidays/Fireworks/fireworks/ba-large-white-firework-gif-pic.gif)